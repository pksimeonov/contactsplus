package com.example.pavel.contactsplus.data;

import android.provider.BaseColumns;

/**
 * Created by Pavel on 13/12/2014.
 */
public class ContactsDbContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public ContactsDbContract() {

    }

    /* Inner class that defines the table contents */
    public static abstract class ContactEntry implements BaseColumns {
        public static final String TABLE_NAME = "contacts";
        public static final String COLUMN_CONTACT_NUMBER = "number";
        public static final String COLUMN_CONTACT_FIRST_NAME = "first_name";
        public static final String COLUMN_CONTACT_SURNAME = "surname";
    }

}
