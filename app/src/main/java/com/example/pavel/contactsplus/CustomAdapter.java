package com.example.pavel.contactsplus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Custom adapter for the contacts list view
 */
public class CustomAdapter extends BaseAdapter {

    private ArrayList contactsList;

    private LayoutInflater layoutInflater;

    static class ViewHolder {
        TextView textViewContactFirstName;
        TextView textViewContactSurname;
        TextView textViewContactPhone;
    }

    public CustomAdapter(Context context, ArrayList contactsList) {
        this.contactsList = contactsList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return contactsList.size();
    }

    @Override
    public Object getItem(int position) {
        return contactsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.list_item_contact, null);
            //Using viewHolder for more efficient code,
            //as using findView to search through the view tree is done only once
            viewHolder = new ViewHolder();
            viewHolder.textViewContactFirstName = (TextView) convertView.findViewById(R.id.text_view_first_name);
            viewHolder.textViewContactSurname = (TextView) convertView.findViewById(R.id.text_view_surname);
            viewHolder.textViewContactPhone = (TextView) convertView.findViewById(R.id.text_view_phone);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Contact contactItem = (Contact) contactsList.get(position);

        viewHolder.textViewContactFirstName.setText(contactItem.getFirstName());
        viewHolder.textViewContactSurname.setText(contactItem.getSurname());
        viewHolder.textViewContactPhone.setText(contactItem.getPhone());

        return convertView;
    }

}
