package com.example.pavel.contactsplus.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pavel.contactsplus.Contact;

import java.util.ArrayList;

/**
 * Database handling methods
 */
public class ContactsDbHelper extends SQLiteOpenHelper {

    private static final String COMMA_SEP = ",";

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "contacts.db";

    public ContactsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_CONTACTS_TABLE = "CREATE TABLE "
                + ContactsDbContract.ContactEntry.TABLE_NAME
                + " ("
                + ContactsDbContract.ContactEntry._ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP
                + ContactsDbContract.ContactEntry.COLUMN_CONTACT_FIRST_NAME
                + " TEXT NOT NULL" + COMMA_SEP
                + ContactsDbContract.ContactEntry.COLUMN_CONTACT_SURNAME
                + " TEXT NOT NULL" + COMMA_SEP
                + ContactsDbContract.ContactEntry.COLUMN_CONTACT_NUMBER
                + " INTEGER NOT NULL"
                + ");";

        sqLiteDatabase.execSQL(SQL_CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // Note that this only fires if you change the version number for your database.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ContactsDbContract.ContactEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addContact(Contact contact) {
        // Gets the data repository in write mode
        SQLiteDatabase db = this.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues contentValues = new ContentValues();
        contentValues.put(
                ContactsDbContract.ContactEntry.COLUMN_CONTACT_NUMBER, contact.getPhone());
        contentValues.put(
                ContactsDbContract.ContactEntry.COLUMN_CONTACT_FIRST_NAME, contact.getFirstName());
        contentValues.put(
                ContactsDbContract.ContactEntry.COLUMN_CONTACT_SURNAME, contact.getSurname());

        db.insert(ContactsDbContract.ContactEntry.TABLE_NAME, null, contentValues);
        db.close(); // Closing database connection
    }

    public ArrayList<Contact> getAllContacts() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Contact> contactsList = new ArrayList<>();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                ContactsDbContract.ContactEntry._ID,
                ContactsDbContract.ContactEntry.COLUMN_CONTACT_FIRST_NAME,
                ContactsDbContract.ContactEntry.COLUMN_CONTACT_SURNAME,
                ContactsDbContract.ContactEntry.COLUMN_CONTACT_NUMBER
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                ContactsDbContract.ContactEntry.COLUMN_CONTACT_SURNAME + " ASC";

        Cursor cursor = db.query(
                ContactsDbContract.ContactEntry.TABLE_NAME,  // The table to query
                projection, // The columns to return
                null, // The columns for the WHERE clause
                null, // The values for the WHERE clause
                null, // don't group the rows
                null, // don't filter by row groups
                sortOrder // The sort order
        );

        // moveToFirst() places the "read position" on the first entry in the results
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Contact contact = new Contact(cursor.getString(1), cursor.getString(2), cursor.getString(3));
            contactsList.add(contact);
            cursor.moveToNext();
        }

        return contactsList;
    }

}
