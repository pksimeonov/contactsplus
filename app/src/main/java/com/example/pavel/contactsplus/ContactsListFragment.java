package com.example.pavel.contactsplus;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.pavel.contactsplus.data.ContactsDbHelper;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsListFragment extends Fragment {

    static ListView contactsListView;
    static ArrayList<Contact> contacts;

    static ContactsDbHelper db = new ContactsDbHelper(MainActivity.getAppContext());

    @Override
    public void onResume() {
        super.onResume();
        updateContactsList();
    }

    public ContactsListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contacts_list, container, false);

        contactsListView = (ListView) rootView.findViewById(R.id.list_view_contacts);

        return rootView;
    }


    public static void updateContactsList() {
        contacts = db.getAllContacts();
        contactsListView.setAdapter(new CustomAdapter(MainActivity.getAppContext(), contacts));
    }


}
