package com.example.pavel.contactsplus;

/**
 * Created by Pavel on 14/12/2014.
 */
public class Contact {

    private Integer id;
    private String firstName;
    private String surname;
    private String phone;

    public Contact(String firstName, String surname, String phone){
        this.firstName = firstName;
        this.surname = surname;
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer ID) {
        this.id = ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
